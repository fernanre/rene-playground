def str2bool(v):
    """
    Converts a string to boolean

    :param v: Value to convert to boolean
    :return: The boolean result
    """
    return v.lower() in ("yes", "true", "t", "1")