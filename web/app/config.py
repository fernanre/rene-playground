import os
from app.utils import str2bool


class BaseConfig(object):

    APP_PORT = int(os.environ['APP_PORT'])
    SECRET_KEY = os.environ['SECRET_KEY']

    """
    Debug and logging configuration
    """
    DEBUG = str2bool(os.environ['DEBUG'])
    TESTING = str2bool(os.environ['TESTING'])

    """
    Whether or not to use the wsgi Proxy Fix
    """
    USE_PROXY = str2bool(os.environ['USE_PROXY'])


