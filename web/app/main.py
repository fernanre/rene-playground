import os
from flask import Flask, abort, request, current_app
from subprocess import call, PIPE, Popen

app = Flask(__name__)

def host_name():
  return Popen("hostname", stdout=PIPE).stdout.read()

@app.route("/")
def hello():
      
    # return "Hello World"
    app_name = os.getenv('APP_NAME', "Flask")
    app_desc = os.getenv('APP_DESC', "Flask is a microframework for Python based on Werkzeug, Jinja 2 and good intentions")

    current_app.logger.debug("Loading index")

    return "Hello from %s.<br/> Data: %s.<br/> Description: %s.<br/> ContainerID: %s" % (
        app_name,
        request.query_string,
        app_desc,
        host_name()
    )

# @app.route("/msg", methods=["POST", "GET"])
# def msg():
#   print("Args %s, %s", (request.args.get('name'), request.args.items))
#   return "MSG Received: %s. <br/> ContainerID: %s" % (request.data if request.data else request.query_string, host_name())

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True)
