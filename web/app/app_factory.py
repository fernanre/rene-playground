import os
from flask import Flask, abort, request, current_app
from werkzeug.contrib.fixers import ProxyFix

from subprocess import call, PIPE, Popen
from app.config import BaseConfig


def create_app(config_filename):
    """
    Factory to create the application using a file

    :param config_filename: The name of the file that will be used for configuration.
    :return: The created application
    """
    app = Flask(__name__)
    app.config.from_object(config_filename)

    if app.config.get('USE_PROXY', False):
        app.wsgi_app = ProxyFix(app.wsgi_app)

    app = Flask(__name__)
    app.config.from_object(BaseConfig)

    def host_name():
        return Popen("hostname", stdout=PIPE).stdout.read()

    @app.route("/")
    def hello():
        # return "Hello World"
        app_name = os.getenv('APP_NAME', "Flask")
        app_desc = os.getenv('APP_DESC',
                             "Flask is a microframework for Python based on Werkzeug, Jinja 2 and good intentions")

        current_app.logger.debug("Loading index")

        return "Hello from %s.<br/> Data: %s.<br/> Description: %s.<br/> ContainerID: %s" % (
            app_name,
            request.query_string,
            app_desc,
            host_name()
        )

    return app
