# Base Openshift + Flask Project

Base project for Openshift Flask projects using a Dockerfile file.

## Openshift configuration 

### Creating the configuration

1. Duplicate `webapp.properties.sample` into `webapp.properties`. (`webapp.properties` MUST NOT BE on the repository).

1. Set the values of this file (It is better to have the values within quotes (`'`)). 

1. Then you have to create the configuration ConfigMap:

    ```
    generate_configmap.py [-h] [-n [configmap_name]] [-f [properties_file]]
    ```
    
1. The output will display a command that you must run (being logged to Openshift) to generate a configmap from literals.


NOTE:
It's important to add this environment variable if using python 3:
```
- name: LD_LIBRARY_PATH
    value: /opt/rh/rh-python34/root/usr/lib64:/opt/rh/httpd24/root/usr/lib64
```  